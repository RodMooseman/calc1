<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    <title>Calculadora</title>
    <?php
      include 'dbc.php';
    ?>
    <style>
    input[type=number]{
    width: 80px;
} 
</style>
  </head>
  <body>
    <div class="container" align="center">
      <header>
		    <div class="alert alert-info">
	        <h2>Calculadora de Precios On-Premise</h2>
        </div>
	    </header>
      <form action='excel.php' method='post' enctype='multipart/form-data'><br>
        Importar archivo para calcular costos : <input type='file' name='somefile' size='20' >
        <input type='submit' name='submit' value='Subir ese archivo'>
      </form>
      <form action='calcular.php' method='post'><br>
        <h3 class="bg-primary text-center pad-basic no-btm">Selecciona los componentes </h3>
        <!--Tipos de maquina: <select id="theMachine" name="theMachine" onchange="giveMachine(this)">
          <option value="0"></option>
        </select>-->
        <input type="hidden" id="childOfMachine" name="childOfMachine" value="1">
        <br><h2>Maquinas : </h2>
        <table width="70%" style="align:center;" class="table bg-info">
          <!-- Renglon   titulos   -->
            <tr>
              <th width="15%">sistema operativo</th>
              <th width="22%">Maquina base</th>
              <th width="13%">Cantidad de maquinas<br>de este tipo</th>
              <th width="13%">vCPU extra (Gb)</th>
              <th width="13%">RAM extra (Gb)</th>
              <th width="16%">Storage extra (Gb)</th>
              <th width="8%"></th>
            </tr>
        </table>
        <table width="70%" style="align:center;" id="tabla" class="table bg-info">
          <tr >
            <td width="15%">
              <select style="width:100%" name="SO0" id="SO0" required onchange="callESO(this,0);">
                <option value=""></option>
                <option value="Windows">Windows</option>
                <option value="Linux">Linux</option>
              </select>
              <select style="width:100%" name="ESO0" id="ESO0" required onchange="callTemplate(this,0);">
                <option value=""></option>
              </select>
            </td>
            <td width="22%">
              <select name="Template0" id="Template0" style="width:100%" required >
                <option value=""></option>
              </select>
            </td>
            <td width="13%"><input type="number"   required onkeypress="return isIntNumber(event)" name="nMachine0" id="nMachine0" autocomplete="off" maxlength="3"  oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" min="1"></td>
            <td width="13%"><input type="number"   required onkeypress="return isIntNumber(event)" name="vCPU0" id="vCPU0" autocomplete="off" maxlength="3"  oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" min="1"></td>
            <td width="13%">
              <select style="width:100%" name="RAM0" id="RAM0" required >
                <option value="0">0</option>
                <?php
                  for ($i=1;$i<129;)
                  {
                    echo '<option value="'.$i.'">'.$i.'</option>';
                    $i=$i*2;
                  }
                ?>
              </select>
            </td>
            <td width="16%">
              <select style="width:100%" name="extStorage0" id="extStorage0"  onchange="callStorage(this,0);">
                <option value="">- No -</option>
                <option value="STANDARD HDD">STANDARD HDD</option>
                <option value="STANDARD SSD">STANDARD SSD</option>
                <option value="PREMIUM SSD">PREMIUM SSD</option>
              </select>
              <select style="width:100%" name="realExtraStorage0" id="realExtraStorage0"  >
                <option value=""></option>
              </select>
            </td>
            <td width="8%" class="eliminar"><input type="button"   value="VM -" onclick="killThisMachine(0);"></td>
          </tr>
          <!-- Placeholder -->
            <tr id="maquinasPlaceholder" ></tr>
        </table>
        <div class="btn-der">
			    <input type="submit" name="insertar" value="Generar costo" class="btn btn-info"/>
		    	<button id="adicional" name="adicional" type="button" class="btn btn-warning" onclick="giveSomeMachine()"> VM + </button>
          <a href="reporte.php?datos=<?php echo $_POST['folio']; ?>">Generar doc</a>
		    </div>
        <br><br>
      </form>
    </div>
  </body>
  <script>
    function callESO(select,number)
    {
      var some;
      if (window.XMLHttpRequest)
      {
        some=new XMLHttpRequest();
      }
      else
      {
        some=new ActiveXObject("Microsoft.XMLHTTP");
      }	
      some.onreadystatechange = function() {
        if(some.readyState == 4 && some.status == 200)
        {
          document.getElementById("ESO"+number).innerHTML = some.responseText;
          document.getElementById("Template"+number).innerHTML = "";
        }
      }
      some.open("GET","thatIsComing.php?some="+select.value, true);
      some.send();
    }
    function callTemplate(select,number)
    {
      var some;
      if (window.XMLHttpRequest)
      {
        some=new XMLHttpRequest();
      }
      else
      {
        some=new ActiveXObject("Microsoft.XMLHTTP");
      }	
      some.onreadystatechange = function() {
        if(some.readyState == 4 && some.status == 200)
        {
          document.getElementById("Template"+number).innerHTML = some.responseText;
        }
      }
      some.open("GET","giveMeTemplate.php?some="+select.value, true);
      some.send();
    }
    function callStorage(select,number)
    {
      var some;
      if (window.XMLHttpRequest)
      {
        some=new XMLHttpRequest();
      }
      else
      {
        some=new ActiveXObject("Microsoft.XMLHTTP");
      }	
      some.onreadystatechange = function() {
        if(some.readyState == 4 && some.status == 200)
        {
          document.getElementById("realExtraStorage"+number).innerHTML = some.responseText;
        }
      }
      some.open("GET","giveMeStorage.php?some="+select.value, true);
      some.send();
    }
    function killThisMachine(number)
    {
      //  recuperar informacion de equipo
        workplace1="maquinasPlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfMachine").value;
        mimic=parseInt(mimic);
      if(mimic>1)
      {
        mimic=mimic-1;
        // eliminar
          if(number!=mimic)
          {
            for(i=number;i<mimic;i++)
            {
              // template
                temp1="Template"+(i+1);
                some=document.getElementById(temp1).value;
                temp1="Template"+i;
                document.getElementById(temp1).value=some;
              // multiplicador
                temp1="nMachine"+(i+1);
                some=document.getElementById(temp1).value;
                temp1="nMachine"+i;
                document.getElementById(temp1).value=some;
              // vCPU
                temp1="vCPU"+(i+1);
                some=document.getElementById(temp1).value;
                temp1="vCPU"+i;
                document.getElementById(temp1).value=some;
              // RAM
                temp1="RAM"+(i+1);
                some=document.getElementById(temp1).value;
                temp1="RAM"+i;
                document.getElementById(temp1).value=some;
              // Storage
                temp1="extStorage"+(i+1);
                some=document.getElementById(temp1).value;
                temp1="extStorage"+i;
                document.getElementById(temp1).value=some;
              // SO
                temp1="SO"+(i+1);
                some=document.getElementById(temp1).value;
                temp1="SO"+i;
                document.getElementById(temp1).value=some;
            }
          }
        document.getElementById("childOfMachine").value=mimic;
        var el ='childMachine'+mimic.toString();
        document.getElementById(el).remove();
      }
      else
        alert("no se puede eliminar elementos, si la lista solo contiene un elemento");
    }
    function giveSomeMachine()
    {
      //  recuperar informacion de equipo
        workplace1="maquinasPlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfMachine").value;
        mimic=parseInt(mimic);
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  añadir SO
            theSOPlace = document.createElement("td");
            theSOPlace.style="width:15%";
            theSO = document.createElement("select");
            theSO.name="SO"+mimic;
            theSO.id="SO"+mimic;
            theSO.required=true; 
            theSO.style="width:100%";
            thoseELM = ["", "Windows", "Linux"];
            for(i=0;i<thoseELM.length;i++)
            {
              var opt = document.createElement('option');
              opt.value = thoseELM[i];
              opt.innerHTML = thoseELM[i];
              theSO.appendChild(opt);
            }
            theSO.onchange = function(){callESO(this,numb)}; 
            theESO = document.createElement("select");
            theESO.name="ESO"+mimic;
            theESO.id="ESO"+mimic;
            theESO.required=true; 
            theESO.style="width:100%";
            theESO.onchange = function(){callTemplate(this,numb)}; 
            theSOPlace.appendChild(theSO);
            theSOPlace.appendChild(theESO);
          //  añadir template
            theTemplatePlace = document.createElement("td");
            theTemplatePlace.style="width:22%";
            theTemplate = document.createElement("select");
            theTemplate.name="Template"+mimic;
            theTemplate.id="Template"+mimic;
            theTemplate.required=true; 
            theTemplate.style="width:100%";
            var opt = document.createElement('option');
            opt.value = "";
            opt.innerHTML = "";
            theTemplate.appendChild(opt);
            theTemplatePlace.appendChild(theTemplate);
          //  añadir multiplicador
            theMultiplicadorPlace = document.createElement("td");
            theMultiplicadorPlace.style="width:13%";
            theMultiplicador = document.createElement("input");
            theMultiplicador.name="nMachine"+mimic;
            theMultiplicador.id="nMachine"+mimic;
            theMultiplicador.required=true; 
            theMultiplicador.type="number";
            theMultiplicador.style="max-width:250px;";
            theMultiplicador.min=1;
            theMultiplicador.step=1;
            theMultiplicadorPlace.appendChild(theMultiplicador);
          //  añadir vCPU
            thevCPUPlace = document.createElement("td");
            thevCPUPlace.style="width:13%";
            thevCPU = document.createElement("input");
            thevCPU.name="vCPU"+mimic;
            thevCPU.id="vCPU"+mimic;
            thevCPU.required=true; 
            thevCPU.type="number";
            thevCPU.style="max-width:250px;";
            thevCPU.min=0;
            thevCPU.step=1;
            thevCPUPlace.appendChild(thevCPU);
          //  añadir RAM
            theRAMPlace = document.createElement("td");
            theRAMPlace.style="width:13%";
            theRAM = document.createElement("select");
            theRAM.name="RAM"+mimic;
            theRAM.id="RAM"+mimic;
            theRAM.required=true; 
            theRAM.style="width:100%";
            opt = document.createElement('option');
            opt.value = 0;
            opt.innerHTML = 0;
            theRAM.appendChild(opt);
            for(i=1;i<129;)
            {
              opt = document.createElement('option');
              opt.value = i;
              opt.innerHTML = i;
              theRAM.appendChild(opt);
              i=i*2;
            }
            theRAMPlace.appendChild(theRAM);
          //  añadir Storage
            theStoragePlace = document.createElement("td");
            theStoragePlace.style="width:13%";
            theStorage = document.createElement("select");
            theStorage.name="extStorage"+mimic;
            theStorage.id="extStorage"+mimic;
            theStorage.style="width:100%";
            thoseELM = ["- No -", "STANDARD HDD", "STANDARD SSD","PREMIUM SSD"];
            for(i=0;i<thoseELM.length;i++)
            {
              var opt = document.createElement('option');
              if(i!=0)
                opt.value = thoseELM[i];
              else
                opt.value = "";
              opt.innerHTML = thoseELM[i];
              theStorage.appendChild(opt);
            }
            theStorage.onchange = function(){callStorage(this,numb)}; 
            therealExtraStorage = document.createElement("select");
            therealExtraStorage.name="realExtraStorage"+mimic;
            therealExtraStorage.id="realExtraStorage"+mimic;
            therealExtraStorage.style="width:100%";
            theStoragePlace.appendChild(theStorage);
            theStoragePlace.appendChild(therealExtraStorage);
          //  añadir boton
            theButtonPlace = document.createElement("td");
            theButtonPlace.style="width:11%";
            theButtonPlace.class="eliminar";
            theButton = document.createElement("input");
            theButton.type="button";
            theButton.name="Button"+mimic;
            theButton.id="Button"+mimic;
            theButton.value="VM -";
            theButton.onclick = function(){killThisMachine(numb)}; 
            theButtonPlace.appendChild(theButton);
          //  agregar datos predefinidos a nuevo line
            newLine.name = "childMachine"+mimic.toString();
            newLine.id = "childMachine"+mimic.toString();
            newLine.appendChild(theSOPlace);
            newLine.appendChild(theTemplatePlace);
            newLine.appendChild(theMultiplicadorPlace);
            newLine.appendChild(thevCPUPlace);
            newLine.appendChild(theRAMPlace);
            newLine.appendChild(theStoragePlace);
            newLine.appendChild(theButtonPlace);
            workplace1.parentNode.insertBefore(newLine,workplace1);
      mimic++;
      document.getElementById("childOfMachine").value=mimic;
    }
  </script>
</html>